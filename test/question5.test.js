const assert = require("assert/strict");
const { test, describe } = require("node:test");

const { decodeB64String, decodeHexString } = require("../src/question5");

describe("decode strings", () => {
  test("decode first string", () => {
    const string1 =
      "4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c20616772" +
      "6567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c" +
      "736f6c61722e636f6d22207061726120736162657220717565206c6c65676173746520612065737461" +
      "2070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f72206175ed212e";
    const decoded = decodeHexString(string1);
    assert.ok(decoded.includes(" "), "value");
  });
  test("decode second string", () => {
    const string1 =
      "U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVuIG1lbnNhamUu";
    const decoded = decodeB64String(string1);
    assert.ok(decoded.includes(" "), "value");
  });
});
