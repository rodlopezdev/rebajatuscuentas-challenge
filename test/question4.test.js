const assert = require("assert/strict");
const { test, describe } = require("node:test");
const { findMatchingPair } = require("../src/question4");

describe("findMatchingPair", async () => {
  test("Single test 1", () => {
    const list = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const target = 11;
    const values = findMatchingPair(list, target);
    const expectedValue = [2, 9];

    assert.notEqual(values, undefined);
    assert.equal(values?.[0], expectedValue[0]);
    assert.equal(values?.[1], expectedValue[1]);
  });
  test("Single test 2", () => {
    const list = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const target = 20;
    const values = findMatchingPair(list, target);
    assert.equal(values, undefined);
  });
  test("Single test 3 - with 1M elements", () => {
    const list = Array.from({ length: 1000000 }, (_, index) => index + 1);
    const target = 134;
    const values = findMatchingPair(list, target);
    const expectedValue = [1, 133];

    assert.notEqual(values, undefined);
    assert.equal(values?.[0], expectedValue[0]);
    assert.equal(values?.[1], expectedValue[1]);
  });
});
