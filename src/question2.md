## Deploy infra automatized with SST

- [SST](https://sst.dev/)
- When deploying resources to AWS, I use SST to automate deployment (Like CloudFormation but with simple TS Code), ensure control of resources, configure them correctly, and allow them to be easily deprovisioned.
- The question2 folder contains 2 files.
- infrastructure.ts is an infrastructure like code definition for implementing a DynamoDB table and an SQS queue.
- sst.config.ts is the main file for running sst commands. You need to install sst and work with TS Code.
- The code is an example based on a real application.

## Previus experience

- I worked with pipelines in jenkins passing properties to the kubernetes context to deploy containers.
- I used github action templates to deploy containers within AWS EKS.