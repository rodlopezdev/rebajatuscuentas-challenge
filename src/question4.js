/**
 * Considerations:
 * - This function return de first match, so if exist 2 or more matches it returns just first
 * - Considering that the input list is ordered, what the function does is take both ends
 *  of the list and go through the elements from the top and bottom
 */
function findMatchingPair(list, target) {
  let startIndex = 0;
  let endIndex = list.length - 1;

  while (startIndex < endIndex) {
    const currentMatch = list[startIndex] + list[endIndex];

    if (currentMatch === target) {
      return [list[startIndex], list[endIndex]];
    }

    if (currentMatch < target) {
      startIndex++;
    } else {
      endIndex--;
    }
  }
  return undefined;
}

module.exports = { findMatchingPair };
