## Webapps projects

### RiverETL

This site is capable of creating integration between different database engines.
Work with connections and sql queries.
Simply put, it is a very simplified AWS Glue with limited functionality.

https://www.riveretl.com/es

This was build over:

- Mongo for database
- NestJS for backend (@nest/\*\*, mongoose)
- NextJS for frontend (Mui, React Context, @auth0)
- Auth0 for auth layer

Deployed on Vercel infraestructure

### Nairakkata

Online course platform, similar to udemy

https://www.nairakkata.com/

This was build over:

- MySQL for database
- ExpressJS for backend (mysql2-client, expressjs, middlewares, jwt)
- NextJS for frontend (Mui, React Context)

## NPM Projects

This is my npm profile

```
https://www.npmjs.com/~rodlopez
```

Where you can find my more usefull packages

#### React Clean Code

This is a group of useful features to improve component code and allow you to build CLEAN components.

https://www.npmjs.com/package/@rodlopez/clean-code

#### React Form Handler

This library works on top of react-hook-form and adds a capability to work with dependencies between inputs, trigger functions, and decouple code from UI components.

https://www.npmjs.com/package/@rodlopez/react-form-handler
