import { SSTConfig } from "sst";
import { BaseInfraestructure } from "./infraestructure";

export default {
  config(_input) {
    return {
      name: "stage",
      region: "us-east-1",
    };
  },
  stacks(app) {
    app.stack(BaseInfraestructure);
  },
} satisfies SSTConfig;
