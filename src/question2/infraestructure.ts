import { Stack, Table, Queue, StackContext } from "sst/constructs";

export function GenerateDynamoTable(stack: Stack) {
  const usersTable = new Table(stack, "Template", {
    fields: {
      uuid: "string",
    },
    primaryIndex: {
      partitionKey: "uuid",
    },
  });
  return {
    templateTable: usersTable,
  };
}

export function GenerateSQS(stack: Stack) {
  const mailValidationQueue = new Queue(stack, "auth-email-validation");
  return { mainQueue: mailValidationQueue };
}

export function BaseInfraestructure({ stack }: StackContext) {
  GenerateDynamoTable(stack);
  GenerateSQS(stack);
}
