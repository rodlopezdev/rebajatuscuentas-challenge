- I used RTFM (Read the F\*\*\*ing Manual) today, working on a personal project I need to learn how it works with AWS S3 data events and how to integrate them into AWS CloudTrail, so the best documentation is in AWS Docs https ://docs.aws .amazon.com/awscloudtrail/latest/userguide/logging-data-events-with-cloudtrail.html. I generally read pages of documentation for libraries, frameworks and technologies, supported by courses on udemy or short how-to videos on youtube. The last documentation I was reading about was about the new features of NextJS 14 and nestjs Dynamic modules.

- I used LMGTFY (Let Me Google That For You) when I had a problem implementing a library in NextJS, it had an error related to a client component trying to render on the server. After trying to solve it manually, I resorted to a Google search, finding a solution on StackOverFlow and also in the official NextJS documentation. When I work in a team I try to collaborate with other members, solving doubts and doing peer programming, some problems that arise are very easy to solve and find directly in the documentation, so I try to give a link to the documentation and show them how to find another possible doubts in it.

- I work primarily from MacOS, but I have a desktop computer running Windows 11.

- My main stack is on JS with TS, but I also have a lot of experience working with Python and Java.
