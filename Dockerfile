FROM node:18

COPY ./src ./src
COPY ./test ./test

RUN node --test test/*.test.js