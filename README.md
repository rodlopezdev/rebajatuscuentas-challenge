# Description

Code executed on Node18, with experimental testing module incorporated in Node.
You don't need additional libraries for readability.

Challenge questions

- [Challenge](BASE_README.md)

Solutions:

- [Question 1](src/question1.md)
- [Question 2](src/question2.md)
- [Question 3](src/question3.md)
- [Question 4](src/question4.js)
- [Question 5](src/question5.js)


# Run Dockerfile

```
docker build -t challenge-image .
```

## Screenshots

- When the tests run successfully, the docker image will build correctly

<img title="a title" alt="Error view" src="https://gitlab.com/rodlopezdev/rebajatuscuentas-challenge/-/raw/main/media/success-build.png?ref_type=heads">


- When the test fails, the Docker build stops and displays a message like this

<img title="a title" alt="Error view" src="https://gitlab.com/rodlopezdev/rebajatuscuentas-challenge/-/raw/main/media/error-build.png?ref_type=heads">